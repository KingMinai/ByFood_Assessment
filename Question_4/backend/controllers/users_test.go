package controllers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func setupRouter() *gin.Engine {
	router := gin.Default()
	return router
}

func TestGetAllUsers(t *testing.T) {
	router := setupRouter()
	w := httptest.NewRecorder()

	router.GET("/users", GetUsers)

	req, _ := http.NewRequest(http.MethodGet, "/users", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Contains(t, w.Body.String(), "users")
}

func TestGetSingleUser(t *testing.T) {
	router := setupRouter()
	w := httptest.NewRecorder()

	router.GET("/user/:userID", GetUser)

	response := `{"user":{"id":"1","firstName":"John","lastName":"Doe","role":"Manager"}}`

	req, _ := http.NewRequest(http.MethodGet, "/user/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, response, w.Body.String())
}

func TestCreateUser(t *testing.T) {
	router := setupRouter()
	w := httptest.NewRecorder()

	// test user creation
	router.POST("/user/create", CreateUser)

	user := User{
		FirstName: "Jack",
		LastName:  "Doe",
		Role:      "Sales",
	}

	jsonValue, _ := json.Marshal(user)

	req, _ := http.NewRequest(http.MethodPost, "/user/create", bytes.NewBuffer(jsonValue))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)
	assert.Contains(t, w.Body.String(), "User created successfully")

	// check if user is created
	router.GET("/users", GetUsers)

	req, _ = http.NewRequest(http.MethodGet, "/users", nil) // get all users
	router.ServeHTTP(w, req)

	assert.Contains(t, w.Body.String(), `"firstName":"Jack","lastName":"Doe","role":"Sales"`) // check if user exists
}

func TestUpadteUser(t *testing.T) {
	router := setupRouter()
	w := httptest.NewRecorder()

	// get user info
	router.GET("/user/2", GetUser)

	req, _ := http.NewRequest(http.MethodGet, "/user/2", nil)
	router.ServeHTTP(w, req)

	initialUserInfo := w.Body.String()

	// update user info
	router.PUT("/user/update/:userID", UpdateUser)

	user := User{
		FirstName: "Jeff",
		LastName:  "Poe",
		Role:      "Manager",
	}

	jsonValue, _ := json.Marshal(user)

	req, _ = http.NewRequest(http.MethodPut, "/user/update/2", bytes.NewBuffer(jsonValue))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Contains(t, w.Body.String(), "User updated successfully")

	// get new user ifo
	router.GET("/user", GetUser)

	req, _ = http.NewRequest(http.MethodGet, "/user/2", nil)
	router.ServeHTTP(w, req)

	// verify update
	assert.NotEqual(t, initialUserInfo, w.Body.String())
}

func TestDeleteUser(t *testing.T) {
	router := setupRouter()
	w := httptest.NewRecorder()

	// get user
	router.GET("/user/:userID", GetUser)
	req, _ := http.NewRequest(http.MethodGet, "/user/1", nil)
	router.ServeHTTP(w, req)

	userInfo := w.Body.String()

	// delete user
	router.DELETE("/user/1", DeleteUser)

	req, _ = http.NewRequest(http.MethodDelete, "/user/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Contains(t, w.Body.String(), "User deleted successfully")

	// check if user is deleted
	router.GET("/users", GetUsers)

	req, _ = http.NewRequest(http.MethodGet, "/users", nil)
	router.ServeHTTP(w, req)

	assert.NotContains(t, w.Body.String(), userInfo)
}
