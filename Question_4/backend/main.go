package main

import (
	"example/byfood_usermanager/controllers"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func setupRouter() *gin.Engine {
	router := gin.Default()

	router.Use(cors.Default())

	router.GET("/api/users", controllers.GetUsers)
	router.GET("/api/user/:userID", controllers.GetUser)
	router.POST("/api/user/create", controllers.CreateUser)
	router.PUT("/api/user/update/:userID", controllers.UpdateUser)
	router.DELETE("/api/user/:userID", controllers.DeleteUser)

	return router
}

func main() {
	router := setupRouter()
	router.Run(":8080")
}
