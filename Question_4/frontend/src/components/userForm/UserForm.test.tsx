import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import UserForm from './UserForm';

test('userform close function', () => {
	const handleClose = jest.fn();

	render(<UserForm type='new' handleClose={handleClose} />);

	fireEvent.click(screen.getByText('< Back'));

	expect(handleClose).toHaveBeenCalled();
});

test('userform input fields with user', () => {
	const handleClose = jest.fn();
	const user = {
		firstName: 'John',
		lastName: 'Smith',
		role: 'Manager',
	};

	render(<UserForm type='edit' handleClose={handleClose} user={user} />);

	expect(screen.getByPlaceholderText('Enter First Name')).toHaveValue('John');
	expect(screen.getByPlaceholderText('Enter Last Name')).toHaveValue('Smith');
	expect(screen.getByPlaceholderText('Enter Role')).toHaveValue('Manager');
});
