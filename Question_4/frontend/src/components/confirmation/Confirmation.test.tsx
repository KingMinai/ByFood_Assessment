import { render, screen } from '@testing-library/react';

import Confirmation from './Confirmation';

test('renders confirmation message', () => {
	console.log = jest.fn();
	render(<Confirmation message='Are you sure?' handleClose={console.log} handleConfirm={console.log} />);
	const linkElement = screen.getByText(/Are you sure?/i);
	expect(linkElement).toBeInTheDocument();
});

test('confirmation cancel runs', () => {
	console.log = jest.fn();
	render(<Confirmation message='Are you sure?' handleClose={console.log} handleConfirm={console.log} />);
	const button = screen.getByText(/Cancel/i);
	button.click();
	expect(console.log).toHaveBeenCalled();
});

test('confirmation confirm runs', () => {
	console.log = jest.fn();
	render(<Confirmation message='Are you sure?' handleClose={console.log} handleConfirm={console.log} />);
	const button = screen.getByText(/Confirm/i);
	button.click();
	expect(console.log).toHaveBeenCalled();
});
