import React, { useState } from 'react';
import { Card } from 'react-bootstrap';

import { deleteUser } from '../../api/api';
import { ActionButton } from '../actionButton/ActionButton';
import Confirmation from '../confirmation/Confirmation';

interface User {
	id: string;
	firstName: string;
	lastName: string;
	role: string;
}

interface Props {
	userInfo: User;
	handlePopup: (type: string, id?: string, user?: User) => void;
	refresh: () => void;
}

export const UserCard: React.FC<Props> = ({ userInfo, handlePopup, refresh }) => {
	const [confirm, setConfirm] = useState(false);

	const handleEdit = () => {
		handlePopup('edit', userInfo.id, userInfo);
	};

	const handleDelete = () => {
		setConfirm(true);
	};

	const dellUser = async () => {
		await deleteUser(userInfo.id);
		setConfirm(false);
		refresh();
	};

	return (
		<>
			<Card style={{ width: '18rem' }}>
				<Card.Body>
					<Card.Title>
						{userInfo.firstName} {userInfo.lastName}
					</Card.Title>
					<Card.Subtitle className='mb-2 text-muted'>{userInfo.role}</Card.Subtitle>
					<ActionButton buttonName='Edit' handleAction={handleEdit} />
					<ActionButton buttonName='Delete' handleAction={handleDelete} />
				</Card.Body>
			</Card>
			{confirm && (
				<Confirmation
					message='Are you sure you want to delete this user?'
					handleConfirm={dellUser}
					handleClose={() => setConfirm(false)}
				/>
			)}
		</>
	);
};

export default UserCard;
