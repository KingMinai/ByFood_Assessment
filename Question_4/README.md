# Basic CRUD user management system

## [Live Demo](http://188.166.161.104/)

## Features
- Master view to list all users
- Form to add new users
- Form to edit existing users
- Button to delete users

## Backend
The backend is a simple REST API built with Golang and Gin. It uses a Sqlite3 database, with Gorm, to store the users.

### API endpoints:
`GET /api/users` - Get all users<br>
`GET /api/user/:id` - Get a user by id<br>
`POST /api/user/create` - Create a new user<br>
`PUT /api/user/:id` - Update a user<br>
`DELETE /api/user/:id` - Delete a user  

## Frontend
The frontend is a simple React app, made with Typescript. It uses the Bootstrap library for styling.

### Components:
`ActionButtons` - Button that triggers a custom action<br>
`UserCard` - Card that displays a user's information<br>
`UserForm` - Form to create or edit a user<br>
`Nav` - Navigation bar<br>
`Confirmation` - Modal to confirm an action<br>
`App` - Main component and master view

