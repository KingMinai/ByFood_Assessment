package main

import (
	"example/byfood_q2/answer"
	"fmt"
)

func main() {
	fmt.Println("Question 2:")
	fmt.Println("Enter a number:")
	var input int
	fmt.Scanf("%d", &input)
	fmt.Println("Ouput:")
	fmt.Println(answer.RecursiveHalf(input))
}
