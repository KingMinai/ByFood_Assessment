# Recursive half

## Problem
Write a recursive function which takes one integer parameter.  Please bear in mind that finding the algorithm needed to generate the output below is the main point of the question. Please do not ask which algorithm to use.


## Example
```go
Input: 9
Output:
2
4
9
```

## Run and test
To run the program go to `Question_2/` directory and run the following command.
```bash
go run main.go
```

### Example Usage
```bash
$ go run main.go 
Question 2:
Enter a number:
24        
Ouput:
[3 6 12 24]
```

To run the tests go to `Question_2/answer/` directory and run the following command.
```bash
go test -v
```

## Solution
Looking at the example and the extra examples I recieved I noticed the out put was the number being divived by 2 and rounded to the lower integer. While googling flooring I realised that divinding an int by 2 in go automatically floors the number. I then realised that I could just keep dividing the number by 2 until it was 1 and then print the number. This is my recursive solution.

```go
func RecursiveHalf(n int) []int {
	if n == 1 {
		return nil
	}
	return append(RecursiveHalf(n/2), n)
}
```

None of the numbers reach 1 in the examples so I firgured that was the ending condition.

[_back to top_](#recursive-half) | [_back to main README_](https://gitlab.com/KingMinai/ByFood_Assessment/-/tree/main)
