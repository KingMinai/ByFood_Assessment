# Find most common string in list

## Problem
Write a function which takes one parameter as an array/list.  Find most repeated data within a given array.  Test with different datasets.

## Example
```go
Input : ["apple","pie","apple","red","red","red"]
Output : "red"
```

## Run and test
To run the program go to `Question_1/` directory and run the following command.
```bash
go run main.go
```

### Example Usage
```bash
$ go run main .go
Question 3:
Enter a list of strings with repeated values, separated by spaces:
red yellow pink yellow blue red black yellow blue red pink red
Most common:
red
```

To run the tests go to `Question_1/answer/` directory and run the following command.
```bash
go test -v
```

## Solution
As the function works only with strings I use `strings.Cout()` to get the number of times a string appears in the list. I then check if a string appears more times than the current most common string I set it as the new most common string. 

```go
func ArrayMode(input []string) string {
	var mode string
	var maxCount int
	var count int
	for _, v := range input {
		count = strings.Count(strings.Join(input, ""), v)
		if count > maxCount {
			maxCount = count
			mode = v
		}
	}
	return mode
}
```

[_back to top_](#find-most-common-string-in-list) | [_back to main README_](https://gitlab.com/KingMinai/ByFood_Assessment/-/tree/main)
