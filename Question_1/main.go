package main

import (
	"bufio"
	"example/byfood_q1/answer"
	"fmt"
	"os"
	"strings"
)

func main() {

	fmt.Println("Question 1:")
	fmt.Println("Enter a list of strings (with and/or wihout a's), separated by spaces:")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	input := scanner.Text()

	fmt.Println("Sorted list:")
	fmt.Println(answer.A_Sorter(strings.Split(input, " ")))
}
