# Sort list by amount of a's in string

## Problem
Write a function that sorts a bunch of words by the number of character “a”s within the word (decreasing order). If some words contain the same amount of character “a”s then you need to sort those words by their lengths.

## Example
```go
Input :
["aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"]
Output :
["aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "kf", "zc", "l"]
```

## Run and test
To run the program go to `Question_1/` directory and run the following command.
```bash
go run main.go
```

### Example Usage
```bash
$ go run main.go
Question 1:
Enter a list of strings (with and/or wihout a), separated by spaces:
eewfwff aaafss asfa aaaaaa sdffis ddif bbif ds a
Sorted list:
[aaaaaa aaafss asfa a eewfwff sdffis bbif ddif ds]
```

To run the tests go to `Question_1/answer/` directory and run the following command.
```bash
go test -v
```

## Solution
As the requirments state that the strings containing a's should come first I decided to split the list into two lists, one with the strings that contain a's and one with the strings that don't. 

```go
	aArray := []string{}
	nArray := []string{}

	// split array
	for _, v := range arr {
		if strings.Contains(v, "a") {
			// array contains a's
			aArray = append(aArray, v)
		} else {
			// array does not contain a's
			nArray = append(nArray, v)
		}
	}
```

This allowed me to easily manage the strings without having to create a complex sorting function. I then created two implementations of bubble sort, one for the strings that contain a's and one for the strings that don't. 

### A string
I just check if the string at `i` has less a's than the string at `j` and if it does I swap them.
```go
func sortA(arr []string) {
	swap := reflect.Swapper(arr)
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr); j++ {
			if strings.Count(arr[i], "a") > strings.Count(arr[j], "a") {
				swap(i, j)
			}
		}
	}
}
```

### Normal string
I first check if the length of the string at `i` is less than the length of the string at `j` and if it is I swap them. If the lengths are equal I check if the string at `i` is "alphabetically" lesser than the string at `j` and if it is I swap them.
```go
func sortNormal(arr []string) {
	swap := reflect.Swapper(arr)
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr); j++ {
			// get the length of the strings
			iLenght := len(arr[i])
			jLenght := len(arr[j])

			if iLenght > jLenght {
				// swap if the length is greater
				swap(i, j)
			} else if iLenght == jLenght {
				// swap if string is "alphabetically" greater
				if arr[i] < arr[j] {
					swap(i, j)
				}
			}
		}
	}
}
```

I run the two sorting functions and then append the two lists together and return the result.
```go
  sortA(aArray)
  sortNormal(nArray)

  // append the two lists together
  aArray = append(aArray, nArray...)

  return aArray
```

[_back to top_](#sort-list-by-amount-of-as-in-string) | [_back to main README_](https://gitlab.com/KingMinai/ByFood_Assessment/-/tree/main)
