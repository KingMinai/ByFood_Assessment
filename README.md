# byFood Take Home Test
A repo for my answers to the byFood assignment. You can find links to the writeups for my answers in the answers section below. Or jump to the [quick start](#quick-start) section to find the commands to run and test the answers.
<br>

## Answers 
### [Question 1](https://gitlab.com/KingMinai/ByFood_Assessment/-/tree/main/Question_1) - An algorithm to sort a list of strings by the amount of a's in them

### [Question 2](https://gitlab.com/KingMinai/ByFood_Assessment/-/tree/main/Question_2) - A recursive algorithm that divides the input by two (rounding down) until it reaches 1

### [Question 3](https://gitlab.com/KingMinai/ByFood_Assessment/-/tree/main/Question_3) - An algorithm to find the most common string in a list

### [Question 4](https://gitlab.com/KingMinai/ByFood_Assessment/-/tree/main/Question_4) - A simple user management web app written in ReactJs using a Golang backend API

## Quick Start
### Requirements
 [Golang](https://golang.org/doc/install) | [NodeJs](https://nodejs.org/en/download/) 

### Question 1-3
For the first three questions, `cd` into the directory of the question and running the following commands.

```bash
# To run the program
go run main.go

# To run the tests
cd answer
go test -v

```

### Question 4
View live demo [here](http://188.166.161.104/).<br>
For the forth question there are two parts, the backend API and the frontend. They both need to be running for the app to work. To setup and run the app, follow the steps below.
#### - Backend API
Run from the `backend` directory
```bash
# API dependencies
go mod download

# Run tests (optional) 
go test -v 

# Run the api (leave this running)
go run main.go 
```

#### - Frontend web app
Run from the `frontend` directory
```bash
# Frontend dependencies
npm install

# Run tests (optional) 
npm test

# Run the frontend (leave this running)
npm start
```

Visit [http://localhost:3000](http://localhost:3000) to view local instance of the app

#### **For more details check out the [writeups](#answers) for each question.**

[_back to top_](#byfood-take-home-test)